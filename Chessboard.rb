#define the size of the board
size = 8
#blank string - this is intentional
board = ""

#x axis
x = 0

#y axis
y = 0

#variable to determine when to add a new line
counter = 0

while x <= size
    8.times {board += "# "}
    x += 1
    if y <= size
        board += "\n"
    end
    y += 1
end

puts board