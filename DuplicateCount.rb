#method called find duplicates, takes in array as an argument
def find_duplicates (array)
    #if the array length with unique values is equal to all elements in the array
    if array.uniq.length == array.length
        puts "No duplicates!"
    else
        puts "There are duplicates"
        #using the find_all method to return all elements from Enumerable.
        #then between the curly brackets, we check each element (e) in the array if there is a duplicate element in the array.
        array.find_all { |e| array.count(e) > 1 }
    end
end

#array defined with duplicate numbers
numbers1 = [1,2,2,2,3,3,4,5]

puts find_duplicates(numbers1)